//Require the necessary discord.js classes
const { Client, GatewayIntentBits } =require('discord.js');
const { config }=require('dotenv');
config();


//create a new client instance
const client=new Client ({ intents: [GatewayIntentBits.Guilds,
GatewayIntentBits.GuildMessages,
GatewayIntentBits.MessageContent]});
const token=process.env.TUTORIAL_BOT_TOKEN;

const prefix ='!';
//when the client is ready, run this code (Only Once)
client.once('ready', () => {
    console.log(`${client.user.tag} has logged in`);
});

client.on("messageCreate", (message) => {
    console.log(message.content);
    if (!message.content.startsWith(prefix) || message.author.bot ) 
    {
        console.log("failed!");
        return;
    }

    const args =message.content.slice(prefix.length).split(/ +/);
    const command=args.shift().toLowerCase();

    if(command === "gg")
    {
        message.channel.send("pong!");

    }

});

//Login to Discord With your clients token
client.login(token);